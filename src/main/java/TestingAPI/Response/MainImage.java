
package TestingAPI.Response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "cdnUrl"
})

public class MainImage {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("cdnUrl")
    private String cdnUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public MainImage withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("cdnUrl")
    public String getCdnUrl() {
        return cdnUrl;
    }

    @JsonProperty("cdnUrl")
    public void setCdnUrl(String cdnUrl) {
        this.cdnUrl = cdnUrl;
    }

    public MainImage withCdnUrl(String cdnUrl) {
        this.cdnUrl = cdnUrl;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public MainImage withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
