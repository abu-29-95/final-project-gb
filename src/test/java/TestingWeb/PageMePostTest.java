package TestingWeb;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PageMePostTest extends AbstractTest {


    @Test
    void postHas() {
        new MyPostsPage(getDriver())
                .loginIn("Nike11", "87ed818ab2");


        Assertions.assertTrue(getDriver().findElement(By.xpath(".//*[@id='app']//img")).isDisplayed());

        Assertions.assertTrue(getDriver().findElement(By.xpath(".//*[@id='app']//h2")).isDisplayed());

        Assertions.assertTrue(getDriver().findElement(By
                .cssSelector("#app  div.description.svelte-127jg4t")).isDisplayed());
    }

    @Test
    void createPost() {
        new MyPostsPage(getDriver()).creatPost("Nike11", "87ed818ab2", "ForTest", "Testing");


        Assertions.assertTrue(getDriver().findElement(By
                .cssSelector("#app  div.container.svelte-tv8alb")).isDisplayed());
    }


    @Test
    void nextPageNavigationTest() {

        new MyPostsPage(getDriver())
                .navigationNextPage("Nike11", "87ed818ab2");

        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.urlToBe("https://test-stand.gb.ru/?page=2"));

        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/?page=2"));


    }

    @Test
    void prevPageNavigationTest() {

        new MyPostsPage(getDriver())
                .navigationPrevPage("Nike11", "87ed818ab2");

        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.urlToBe("https://test-stand.gb.ru/?page=1"));

        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/?page=1"));
    }
}
