package TestingWeb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;




public class LogInTest extends AbstractTest{

    @Test
    void logInWithValidDataTest() {
        new MyPostsPage(getDriver())
                .loginIn("Nike11", "87ed818ab2");

        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/"));
    }

    @Test
    void logInWithInvalidDataTest(){
        new MyPostsPage(getDriver())
                .loginIn("Ni", "77ed818");

        Assertions.assertTrue(getDriver().findElement(By.cssSelector("#app div.error-block.svelte-uwkxn9"))
                .isDisplayed());
    }

    @Test
    void logIsMoreThan20(){
        new MyPostsPage(getDriver())
                .loginIn("Test12345678901234567", "0911b34c97");

        Assertions.assertTrue(getDriver().findElement(By.cssSelector("#app div.error-block.svelte-uwkxn9"))
                .isDisplayed());
    }

    @Test
    void logIs20Cher(){
        new MyPostsPage(getDriver())
                .loginIn("Nike1234567890123456", "f838e7cc1e");

        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/"));
    }

    @Test
    void logIs3Cher(){
        new MyPostsPage(getDriver())
                .loginIn("Tes", "78ac122664");

        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/"));
    }





}
