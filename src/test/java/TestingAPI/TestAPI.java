package TestingAPI;

import TestingAPI.Response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;



public class TestAPI extends AbstractTest {


    @Test
    void getPostNotMeCreatedAtAscPage1 () {
        Response response = given().spec(getRequestSpecification())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
       Assertions.assertFalse(response.getData().isEmpty(), "Нету постов");

    }
    @Test
    void getPostNotMeCreatedAtDescPage9999 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "9999")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        Assertions.assertTrue(response.getData().size()>0 || response.getData().size() == 0);
        Assertions.assertEquals(9998, response.getMeta().getPrevPage());

    }

    @Test
    void getPostNotMeCreatedAtDescPage1445 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1445")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        Assertions.assertTrue(response.getData().size()>0 || response.getData().size() == 0);
        Assertions.assertNotNull(response.getMeta().getPrevPage());
        Assertions.assertNotNull(response.getMeta().getNextPage(), "Next page is null");
    }


    @Test
    void getPostNotMeCreatedAtAscPage999999999999 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "999999999999")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        Assertions.assertTrue(response.getData().isEmpty());
        Assertions.assertNotNull(response.getMeta().getPrevPage());
        Assertions.assertNull(response.getMeta().getNextPage(), "Next page not null");

    }

    @Test
    void getPostNotMeCreatedAtALL (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        Assertions.assertTrue(response.getData().isEmpty());

    }

    @Test
    void getPostCreatedAtAscPage1 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page","1")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        assertThat(response.getData().get(0).getId(), equalTo(10923));
        assertThat(response.getData().get(1).getId(), equalTo(10925));
        assertThat(response.getData().get(2).getId(), equalTo(10926));
        assertThat(response.getData().get(3).getId(), equalTo(10927));
        assertThat(response.getData().get(0).getTitle(), containsString("Эверест"));

    }

    @Test
    void getPostCreatedAtDescPage1 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page","1")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        assertThat(response.getMeta().getNextPage(), equalTo(2));
        Assertions.assertFalse(response.getData().isEmpty());
    }

    @Test
    void getPostCreatedAtAscPage2 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page","2")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        assertThat(response.getData().get(0).getId(), equalTo(10928));
        assertThat(response.getData().get(1).getId(), equalTo(10929));
        assertThat(response.getData().get(2).getId(), equalTo(10930));
        assertThat(response.getData().get(3).getId(), equalTo(10933));
        assertThat(response.getData().get(3).getContent(), containsString("Тестирование"));
        Assertions.assertNull(response.getMeta().getNextPage());

    }

    @Test
    void getPostCreatedAtDescPage2 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page","2")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        assertThat(response.getData().get(0).getId(), equalTo(10927));
        assertThat(response.getData().get(1).getId(), equalTo(10926));
        assertThat(response.getData().get(2).getId(), equalTo(10925));
        assertThat(response.getData().get(3).getId(), equalTo(10923));
        Assertions.assertNull(response.getMeta().getNextPage());
    }

    @Test
    void getPostCreatedAtDescPage90000000009 (){
        Response response = given().spec(getRequestSpecification())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page","90000000009")
                .when()
                .get(getBaseUrl())
                .then().spec(getResponseSpecification())
                .extract()
                .response()
                .body()
                .as(Response.class);
        Assertions.assertTrue(response.getData().isEmpty());
        Assertions.assertNotNull(response.getMeta().getPrevPage());
        Assertions.assertNull(response.getMeta().getNextPage());
    }


}
