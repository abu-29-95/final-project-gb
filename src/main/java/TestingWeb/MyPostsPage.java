package TestingWeb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyPostsPage extends AbstractPage{
    @FindBy(xpath= ".//*[@id='login']//input[@type='text']")
    private WebElement login;

    @FindBy(xpath = "//*[@id='login']//input[@type='password']")
    private WebElement password;

    @FindBy(xpath = ".//*[@id='login']//button")
    private WebElement submit;

    @FindBy(id = "create-btn")
    private WebElement buttonCreatedNewPost;

    @FindBy(xpath = ".//*[@id='create-item']//input[@type='text']")
    private WebElement title;

    @FindBy(xpath = ".//*[@id='create-item']//div[2]//label ")
    private WebElement description;

    @FindBy(xpath=".//*[@id='create-item']//button[@type='submit']")
    private WebElement save;

    @FindBy(xpath = ".//*[@id='app']//a[@href='/?page=2']")
    private WebElement nextPage;

    @FindBy(xpath = "//*[@id='app']//a[@href='/?page=1']")
    private WebElement prevPage;

    private WebDriverWait webDriverWait;

    @FindBy(xpath = ".//*[@id='app']//li[3]/a/text()[2]")
    private WebElement welcom;

    public WebElement getWelcom() {
        return welcom;
    }

    public MyPostsPage(WebDriver driver){
        super(driver);
    }

    public void loginIn(){
        this.submit.click();
    }

    public MyPostsPage setLogin(String login){
        this.login.click();
        this.login.sendKeys(login);
        return this;
    }

    public MyPostsPage setPassword(String password){
        this.password.click();
        this.password.sendKeys(password);
        return this;
    }

    public MyPostsPage setTitle(String title) {
        this.title.click();
        this.title.sendKeys(title);
        return this;
    }

    public MyPostsPage setDescription(String  description) {
        this.description.click();
        this.description.sendKeys(description);
        return this;
    }

    public void loginIn(String login, String password){

        Actions loginIn = new Actions(getDriver());
        loginIn
                .sendKeys(this.login,login)
                .click(this.password)
                .sendKeys(password)
                .click(this.submit)
                .build()
                .perform();
    }

    public void creatPost(String login, String password,String title, String description){
        setLogin(login);
        setPassword(password);
        submit.click();

        buttonCreatedNewPost.click();
        setTitle(title);
        setDescription(description);
        save.click();

    }

    public void navigationNextPage(String login, String password){
        setLogin(login);
        setPassword(password);
        submit.click();

        nextPage.click();

    }

    public void navigationPrevPage(String login, String password){
        setLogin(login);
        setPassword(password);
        submit.click();
        this.nextPage.click();
        this.prevPage.click();

    }

}
