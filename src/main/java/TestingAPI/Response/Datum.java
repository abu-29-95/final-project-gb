
package TestingAPI.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "description",
    "content",
    "authorId",
    "mainImage",
    "updatedAt",
    "createdAt",
    "labels",
    "delayPublishTo",
    "draft"
})

public class Datum {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("content")
    private String content;
    @JsonProperty("authorId")
    private Integer authorId;
    @JsonProperty("mainImage")
    private MainImage mainImage;
    @JsonProperty("updatedAt")
    private String updatedAt;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("labels")
    private List<Object> labels = null;
    @JsonProperty("delayPublishTo")
    private Object delayPublishTo;
    @JsonProperty("draft")
    private Boolean draft;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Datum withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Datum withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Datum withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("content")
    public String getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(String content) {
        this.content = content;
    }

    public Datum withContent(String content) {
        this.content = content;
        return this;
    }

    @JsonProperty("authorId")
    public Integer getAuthorId() {
        return authorId;
    }

    @JsonProperty("authorId")
    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Datum withAuthorId(Integer authorId) {
        this.authorId = authorId;
        return this;
    }

    @JsonProperty("mainImage")
    public MainImage getMainImage() {
        return mainImage;
    }

    @JsonProperty("mainImage")
    public void setMainImage(MainImage mainImage) {
        this.mainImage = mainImage;
    }

    public Datum withMainImage(MainImage mainImage) {
        this.mainImage = mainImage;
        return this;
    }

    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Datum withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Datum withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("labels")
    public List<Object> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<Object> labels) {
        this.labels = labels;
    }

    public Datum withLabels(List<Object> labels) {
        this.labels = labels;
        return this;
    }

    @JsonProperty("delayPublishTo")
    public Object getDelayPublishTo() {
        return delayPublishTo;
    }

    @JsonProperty("delayPublishTo")
    public void setDelayPublishTo(Object delayPublishTo) {
        this.delayPublishTo = delayPublishTo;
    }

    public Datum withDelayPublishTo(Object delayPublishTo) {
        this.delayPublishTo = delayPublishTo;
        return this;
    }

    @JsonProperty("draft")
    public Boolean getDraft() {
        return draft;
    }

    @JsonProperty("draft")
    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public Datum withDraft(Boolean draft) {
        this.draft = draft;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Datum withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
