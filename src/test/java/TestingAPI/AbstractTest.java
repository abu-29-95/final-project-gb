package TestingAPI;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractTest {
    private static Properties prop= new Properties();

    private static InputStream configFile;

    private static String baseUrl;

    private static String apiKey;

    private static ResponseSpecification responseSpecification;
    private static RequestSpecification requestSpecification;

  @BeforeAll
    static void initTest () throws IOException {
      configFile = new FileInputStream("src/main/resources/forTestingAPI.properties");

      prop.load(configFile);

      apiKey = prop.getProperty("X-Auth-Token");
      baseUrl = prop.getProperty("baseUrl");


      requestSpecification = new RequestSpecBuilder()
              .addHeader("X-Auth-Token", getApiKey())
              .setContentType(ContentType.JSON)
              .log(LogDetail.ALL)
              .build();

      responseSpecification = new ResponseSpecBuilder()
              .expectStatusCode(200)
              .build();

  }

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static ResponseSpecification getResponseSpecification() {
        return responseSpecification;
    }

    public static RequestSpecification getRequestSpecification() {
        return requestSpecification;
    }
}
